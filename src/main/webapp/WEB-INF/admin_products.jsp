<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="include/header.jsp"%>
<%@ include file="include/navbar.jsp"%>

<div class="container">

    <h1>List of productList</h1>
    <a href="/admin/products/addedit/" class="btn btn-success">Add new product</a><br/><br/>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-hover table-bordered">
                <thead>
                <tr>
                    <th class="text-center">Id</th>
                    <th class="text-center">Photo</th>
                    <th class="text-center">Name</th>
                    <th class="text-center">Category</th>
                    <th class="text-center col-md-1">Price</th>
                    <th class="text-center col-md-1">Quantity</th>
                    <th class="text-center">Description</th>
                    <th class="text-center col-md-1">Action</th>
                </tr>
                </thead>
                <tbody>

                <c:forEach items="${productList}" var="product">
                    <tr>
                        <td>${product.id}</td>
                        <td><img src="${product.photoUrl}" class="img-rounded" alt="Cinque Terre" height="100"></td>
                        <td>${product.name}</td>
                        <td>${product.category}</td>
                        <td>${product.price}</td>
                        <td>${product.quantity}</td>
                        <td>${product.description}</td>
                        <td>
                            <a href="/admin/products/addedit/${product.id}" type="submit" class="btn btn-sm btn-success btn-sm">Edit</a>

                            <form action="/admin/products/delete" method="post">
                                <input name="id" type="hidden" value="${product.id}">
                                <button type="submit" class="btn btn-sm btn-danger btn-sm">Delete</button>
                            </form>
                        </td>
                    </tr>
                </c:forEach>

                </tbody>
            </table>
        </div>
    </div>

</div>
