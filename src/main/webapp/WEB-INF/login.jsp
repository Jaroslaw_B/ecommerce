<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="include/header.jsp"%>

<c:url value="/login" var="loginUrl"/>

<body>
<br/><br/>
<div class="container center-block">
    <div class="col-sm-6 col-md-offset-3">
        <div class="form-top">
            <div class="form-top-left">
                <h3>Sign up now</h3>
                <p>Fill in the form below to get access to our shop:</p>
                <c:if test="${param.error != null}">
                    <p class="bg-danger">
                        Invalid username and password.
                    </p>
                </c:if>
                <c:if test="${param.logout != null}">
                    <p class="bg-danger">
                        You have been logged out.
                    </p>
                </c:if>
            </div>
        </div>
        <div class="form-bottom">
            <form action="${loginUrl}" method="post" class="registration-form">
                <div class="form-group">
                    <label class="sr-only" for="email">User</label>
                    <input type="text" name="email" class="form-control" id="email">
                </div>
                <div class="form-group">
                    <label class="sr-only" for="password">Password</label>
                    <input type="text" name="password" class="form-control" id="password">
                </div>
                <button type="submit" class="btn">Login</button>
                <span class="pull-right">Dont't have account? <a href="/register">Go to registration form!</a></span>
            </form>
        </div>
    </div>
</div>
</body>
</html>