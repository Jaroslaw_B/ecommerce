<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="include/header.jsp"%>
<%@include file="include/navbar.jsp"%>


<c:url value="/products" var="categoryUrl"/>
<c:url value="/product" var="productUrl"/>

<div class="container">
    <div class="row">
        <div class="col-md-3">
            <p class="lead">Shop Name</p>
            <div class="list-group">
                <a href="/" class="list-group-item">All categories</a>
                <c:forEach items="${categories}" var="category">
                    <a href="${categoryUrl}/${category}" class="list-group-item">${category}</a>
                </c:forEach>
            </div>
        </div>

        <div class="col-md-9">

            <div class="row carousel-holder">

                <div class="col-md-12">
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="item active">
                                <img class="slide-image" src="http://placehold.it/800x300" alt="">
                            </div>
                            <div class="item">
                                <img class="slide-image" src="http://placehold.it/800x300" alt="">
                            </div>
                            <div class="item">
                                <img class="slide-image" src="http://placehold.it/800x300" alt="">
                            </div>
                        </div>
                        <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </a>
                    </div>
                </div>

            </div>

            <div class="row">
                <c:forEach items="${productList}" var="product">

                <div class="col-sm-4 col-lg-4 col-md-4">
                    <div class="thumbnail">
                        <img src="${product.photoUrl}" alt="">
                        <div class="caption">
                            <h4 class="pull-right">${product.price}$</h4>
                            <h4>${product.name}
                            </h4>

                            <%--<a class="pull-left btn btn-success" href="${productUrl}?id=${product.id}">Details</a>--%>
                            <form class="pull-left" action="${productUrl}" method="get">
                                <input name="id" type="hidden" value="${product.id}">
                                <button type="submit" class="btn btn-danger">Details</button>
                            </form>
                            <form class="pull-right" action="/shoppingcart/add" method="post">
                                <input name="id" type="hidden" value="${product.id}">
                                <button type="submit" class="btn btn-success">Add to cart</button>
                            </form>
                        </div>
                    </div>
                </div>
                </c:forEach>
            </div>
        </div>
    </div>
</div>
<%@ include file="include/footer.jsp"%>


