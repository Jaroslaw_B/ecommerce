<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<c:url var="homeUrl" value="/"/>
<c:url var="logoutUrl" value="/logout" />
<c:url var="termsUrl" value="/terms"/>
<c:url var="contactUrl" value="/contact"/>
<c:url var="shoppingcartUrl" value="/shoppingcart"/>
<c:url var="loginUrl" value="/login"/>
<c:url var="myAccountUrl" value="/myaccount"/>



<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="${homeUrl}"><i class="glyphicon glyphicon-home"></i> Main</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li>
                    <a href="${termsUrl}"><i class="glyphicon glyphicon-list-alt"></i> Terms of use</a>
                </li>

                <li>
                    <a href="${contactUrl}"><i class="glyphicon glyphicon-envelope"></i> Contact</a>
                </li>
                <sec:authorize access="hasRole('ADMIN')">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        Admin Panel
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="/admin/products">List of products</a>
                        </li>
                        <li role="separator" class="divider"></li>
                        <li>
                            <a href="/admin/orders">List of orders</a>
                        </li>
                        <li role="separator" class="divider"></li>
                        <li>
                            <a href="/admin/users">List of users</a>
                        </li>
                        <li role="separator" class="divider"></li>
                        <li>
                            <a href="/admin/newsletter">Newsletter</a>
                        </li>
                    </ul>
                </li>
                </sec:authorize>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="${shoppingcartUrl}"><i class="glyphicon glyphicon-shopping-cart"></i> Shopping cart</a>
                </li>
                <sec:authorize access="isAnonymous()">
                <li>
                    <a href="${loginUrl}"><i class="glyphicon glyphicon-log-in"></i> Login</a>
                </li>
                </sec:authorize>
                <sec:authorize access="hasAnyRole('ADMIN', 'USER')">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <i class="glyphicon glyphicon-user"></i> <sec:authentication property="principal.username" />
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="${myAccountUrl}"><i class="glyphicon glyphicon-cog"></i> My account</a>
                        </li>
                        <li role="separator" class="divider"></li>
                        <li>
                            <a href="${logoutUrl}"><i class="glyphicon glyphicon-log-out"></i> Log out</a>
                        </li>
                    </ul>
                </li>
                </sec:authorize>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>