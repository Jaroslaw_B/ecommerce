<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="include/header.jsp"%>
<%@ include file="include/navbar.jsp"%>
<div class="container">
    <h1>List of orders</h1>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-hover table-bordered">
                <thead>
                <tr>
                    <th class="text-center">User details</th>
                    <th class="text-center">Ordered products</th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                        <table>
                              <tr>
                                  <td>Name: </td>
                                  <td>${order.user.fullName}</td>
                              </tr>
                              <tr>
                                  <td>Address: </td>
                                  <td>${order.user.address}</td>
                              </tr>
                              <tr>
                                  <td>Email: </td>
                                  <td>${order.user.email}</td>
                              </tr>
                        </table>
                        </td>
                        <td>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th class="text-center">Name</th>
                                    <th class="text-center">Price</th>
                                </tr>
                            </thead>
                            <c:forEach items="${order.productList}" var="product">
                                 <tr>
                                     <td>${product.name}</td>
                                     <td>${product.price}</td>
                                 </tr>
                             </c:forEach>
                        </table>
                            Total ammount: ${order.ammount}
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

</div>
