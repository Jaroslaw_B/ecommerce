<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="include/header.jsp"%>
<%@include file="include/navbar.jsp"%>
<div class="container">

    <h1>Shopping cart</h1>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-hover table-bordered">
                <thead>
                <tr>
                    <th class="text-center">Name</th>
                    <th class="text-center col-md-1">Price</th>
                    <th class="text-center col-md-1">Remove</th>
                </tr>
                </thead>
                <tbody>

                <c:forEach items="${cart}" var="product" varStatus="loop">
                    <tr>
                        <td>${product.name}</td>
                        <td>${product.price}</td>
                        <td>
                            <form action="/shoppingcart/delete" method="post">
                                <input name="index" type="hidden" value="${loop.index}">
                                <button type="submit" class="btn btn-danger"><i class="glyphicon glyphicon-trash"></i></button>
                            </form>
                        </td>
                    </tr>
                </c:forEach>

                </tbody>

            </table>
            <a href="/order" class="pull-right btn btn-success btn-group-lg">Order</a>
            <a href="/shoppingcart/clear" class="pull-right btn btn-danger btn-group-lg">Clear</a>
        </div>
    </div>
</div>

