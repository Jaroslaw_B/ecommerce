<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="include/header.jsp"%>
<%@ include file="include/navbar.jsp"%>
<div class="container">

    <h1>List of users</h1>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-hover table-bordered">
                <thead>
                <tr>
                    <th class="text-center col-md-1">Id</th>
                    <th class="text-center">Email</th>
                    <th class="text-center">Full Name</th>
                    <th class="text-center">Address</th>
                    <th class="text-center col-md-1">Role</th>
                    <th class="text-center col-md-1">Newsletter</th>
                    <th class="text-center col-md-1">Action</th>
                </tr>
                </thead>
                <tbody>

                <c:forEach items="${users}" var="user">
                    <tr>
                        <td>${user.id}</td>
                        <td>${user.email}</td>
                        <td>${user.fullName}</td>
                        <td>${user.address}</td>
                        <td>${user.role}</td>
                        <td>
                            <c:choose>
                                <c:when test="${user.nwsltr == true}">
                                    <span class="glyphicon glyphicon-ok"></span>
                                </c:when>
                                <c:when test="${user.nwsltr == false}">
                                    <span class="glyphicon glyphicon-remove"></span>
                                </c:when>
                            </c:choose> 
                        </td>
                        <td>
                            <a href="/admin/users/${user.id}" type="submit" class="btn btn-sm btn-success btn-sm">Edit</a>
                            <form action="/admin/users/delete" method="post">
                                <input name="id" type="hidden" value="${user.id}">
                                <button type="submit" class="btn btn-sm btn-danger btn-sm">Delete</button>
                            </form>
                        </td>
                    </tr>
                </c:forEach>

                </tbody>
            </table>
        </div>
    </div>
</div>
