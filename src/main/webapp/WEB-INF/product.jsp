<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="include/header.jsp"%>
<%@ include file="include/navbar.jsp"%>

<c:url value="/products" var="categoryUrl"/>
<c:url value="/product" var="productUrl"/>
<c:url value="/review" var="submitReview"/>
<div class="container">

    <div class="row">

        <div class="col-md-3">
            <p class="lead">Shop Name</p>
            <div class="list-group">
                <a href="/" class="list-group-item">All categories</a>
            </div>
        </div>

        <div class="col-md-9">
            <div class="thumbnail-product">
                <div>
                    <a href="${product.photoUrl}"><img class="img-responsive" src="${product.photoUrl}" alt=""></a>
                </div>
                <div class="caption-full">
                    <h4 class="pull-right">${product.price}</h4>
                    <h4><a href="#">${product.name}</a>
                    </h4>
                    <p>${product.description}</p>
                </div>
                <div class="ratings" style="min-height: 30px;">
                    <c:set var="numberOfReviews" value="${fn:length(reviews)}"/>
                    <c:set var="sumRate" value="${0}"/>
                    <c:forEach var="review" items="${reviews}">
                        <c:set var="sumRate" value="${sumRate + review.rate}" />
                    </c:forEach>
                    <c:set var="averageRate" value="${sumRate/numberOfReviews}"/>
                    <p class="pull-right">${numberOfReviews} reviews</p>
                    <p>
                        <c:if test="${numberOfReviews>0}">
                        <c:forEach var="i" begin="1" end="5">
                            <c:if test="${i<=averageRate}"><span class="glyphicon glyphicon-star"></span></c:if>
                            <c:if test="${i>averageRate}"><span class="glyphicon glyphicon-star-empty"></span></c:if>
                        </c:forEach>
                        ${averageRate}</c:if>
                    </p>
                </div>
            </div>

            <div class="well">
                <div style="display: none;" id="reviewForm">
                    <form role="form" action="${submitReview}/${product.id}" method="post">

                        <textarea name="text" style="width:450px;height:150px;"></textarea><br/>

                        <div class="rating" style="margin-left: 200px;">
                            <input type="radio" id="star5" name="rate" value="5" /><label for="star5" title="Rocks!">5 stars</label>
                            <input type="radio" id="star4" name="rate" value="4" /><label for="star4" title="Pretty good">4 stars</label>
                            <input type="radio" id="star3" name="rate" value="3" /><label for="star3" title="Average">3 stars</label>
                            <input type="radio" id="star2" name="rate" value="2" /><label for="star2" title="Bad">2 stars</label>
                            <input type="radio" id="star1" name="rate" value="1" /><label for="star1" title="Sucks">1 star</label>
                        </div>
                        <button class="btn" type="submit" >Submit</button>
                    </form>
                </div>

                <div class="text-right">
                    <a onclick="toggle_visibility('reviewForm')" class="btn btn-success">Leave a Review</a>
                </div>

                <hr>
                <c:forEach items="${reviews}" var="review">
                <div class="row">
                    <div class="col-md-12">
                        <div class="pull-left">
                            <c:forEach var="i" begin="1" end="5">
                                <c:if test="${i<=review.rate}"><span class="glyphicon glyphicon-star"></span></c:if>
                                <c:if test="${i>review.rate}"><span class="glyphicon glyphicon-star-empty"></span></c:if>
                            </c:forEach>
                        </div>
                        <div class="pull-right">${review.user.email}</div>
                    </div>
                    <div>
                        <span>${review.text}</span>
                    </div>
                </div>
                 <hr>
                </c:forEach>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function toggle_visibility(id)
    {
        var e = document.getElementById(id);
        if (e.style.display == 'none')
        {
            e.style.display = 'block';
        }
        else
        {
            e.style.display = 'none';
        }
    }
</script>
<%@ include file="include/footer.jsp"%>