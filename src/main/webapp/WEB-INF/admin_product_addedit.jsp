<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="include/header.jsp"%>
<%@ include file="include/navbar.jsp"%>

<div class="container">

    <h1>Add / Edit product</h1>
    <div class="row">
        <form role="form" enctype="multipart/form-data" class="form-horizontal" action="/admin/products/s/save" method="POST">
            <input type="hidden" value="${product.id}" name="id">

            <div class="form-group">
                <label class="control-label col-sm-2" for="name">Name</label>
                <div class="col-sm-6">
                    <input type="text" value = "${product.name}" name="name" id="name" class="form-control" placeholder="Enter name" autofocus>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="category">Category</label>
                <div class="col-sm-6">
                    <input type="text" value="${product.category}" name="category" id="category" class="form-control" placeholder="Enter last name" autofocus>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="price">Price</label>
                <div class="col-sm-6">
                    <input type="text" value="${product.price}" name="price" id="price" class="form-control" placeholder="Enter price" autofocus>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="quantity">Quantity</label>
                <div class="col-sm-6">
                    <input type="text" value="${product.quantity}" name="quantity" id="quantity" class="form-control" placeholder="Enter quantity" autofocus>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">Photo Url</label>
                <div class="col-sm-6">
                    <input type="file" name="file" class="form-control" placeholder="Select photo" autofocus>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-6">
                    <button type="submit" class="btn btn-primary">Update</button>
                    <a href="\admin\products" class="btn btn-danger">Cancel</a>
                </div>
            </div>
        </form>
    </div>

</div>
