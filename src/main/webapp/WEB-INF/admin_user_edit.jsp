<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="include/header.jsp"%>
<%@ include file="include/navbar.jsp"%>

<div class="container">
    <h1>Edit user</h1>
    <div class="row">
        <form role="form" class="form-horizontal" action="/admin/users/save" method="POST">
            <input type="hidden" value="${user.id}" name="id">
            <div class="form-group">
                <label class="control-label col-sm-2" for="email">Email</label>
                <div class="col-sm-6">
                    <input type="text" value = "${user.email}" name="email" id="email" class="form-control" placeholder="Enter name" autofocus>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="fullName">Full Name</label>
                <div class="col-sm-6">
                    <input type="text" value="${user.fullName}" name="fullName" id="fullName" class="form-control" placeholder="Enter last name" autofocus>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="address">Address</label>
                <div class="col-sm-6">
                    <input type="text" value="${user.address}" name="address" id="address" class="form-control" placeholder="Enter price" autofocus>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="nwsltr">Newsletter accept</label>
                <div class="col-sm-6">
                    <select id="nwsltr" name="nwsltr">
                        <option value=true>ACCEPT</option>
                        <option value=false>DECLINE</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="role">Role</label>
                <div class="col-sm-6">
                    <select id="role" name="role">
                        <option value="ADMIN">ADMIN</option>
                        <option value="USER">USER</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-6">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="\admin\users" class="btn btn-danger">Cancel</a>
                </div>
            </div>
        </form>
    </div>
</div>
