<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="include/header.jsp"%>
<%@ include file="include/navbar.jsp"%>
<c:url value="/order/confirm" var="confirmOrder"/>
<c:url value="/admin/order/details" var="orderDetails"/>

<div class="container">
    <h1>List of orders</h1>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-hover table-bordered">
                <thead>
                <tr>
                    <th class="text-center col-md-1">Id</th>
                    <th class="text-center">Order date</th>
                    <th class="text-center">User</th>
                    <th class="text-center col-md-1">Status</th>
                    <th class="text-center col-md-1">Confirmation</th>
                    <th class="text-center col-md-1">Details</th>

                </tr>
                </thead>
                <tbody>
                <c:forEach items="${orderList}" var="order">
                    <tr>
                        <td>${order.id}</td>
                        <td>${order.orderDate}</td>
                        <td>${order.user.fullName} </td>
                        <td>${order.status}</td>
                        <td class="text-center">
                            <c:if test="${order.status!='ACCEPTED'}">
                                <form action="${confirmOrder}" method="post">
                                    <input name="id" type="hidden" value="${order.id}">
                                    <button type="submit" class="btn btn-group-sm btn-danger">Confirm</button>
                                </form>
                            </c:if>
                        </td>
                        <td>
                            <a href="${orderDetails}/${order.id}" class="btn btn-group-lg btn-success">Details</a>
                        </td>
                    </tr>
                </c:forEach>

                </tbody>
            </table>
        </div>
    </div>

</div>