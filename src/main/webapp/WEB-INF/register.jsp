<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="include/header.jsp"%>
<c:url var="registerUser" value="/register"/>
<body>
<br/><br/>
    <div class="container center-block">
        <div class="col-sm-6 col-md-offset-3">
            <div class="form-top">
                <div class="form-top-left">
                    <h3>Sign up now</h3>
                    <p>Fill in the form below to get access to our shop:</p>
                </div>
            </div>
            <div class="form-bottom">
                <form role="form" action="${registerUser}" method="post" class="registration-form">
                    <div class="form-group">
                        <label class="sr-only" for="fullName">Full name</label>
                        <input type="text" name="fullName" placeholder="Full name..." class="form-control" id="fullName">
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="address">Address</label>
                        <input type="text" name="address" placeholder="Last name..." class="form-control" id="address">
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="email">Email</label>
                        <input type="text" name="email" placeholder="Email..." class="form-control" id="email">
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="password">Password</label>
                        <input type="text" name="password" placeholder="Password..." class="form-control" id="password">
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="form-password2">Re-type password</label>
                        <input type="text" name="form-password2" placeholder="Re-type password..." class="form-control" id="form-password2">
                    </div>
                    <div class="form-group">
                        <input name="nwsltr" type="checkbox" id="nwsltr" value="1">
                        <label for="nwsltr">Newsletter agreement</label>
                    </div>
                    <button type="submit" class="btn">Register!</button>
                    <span class="pull-right">No thanks, <a href="/">back to main</a></span>
                </form>
            </div>
            <div class="model-erros">
                <c:forEach items="${errors}" var="error">

                    <div>${error.code}</div>

                </c:forEach>
            </div>
        </div>

    </div>

</body>
</html>