package com.webstore.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * Created by Jarpen on 2016-07-26.
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter{

    @Autowired
    private UserDetailsService userDetailsService;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        //auth.inMemoryAuthentication().withUser("user").password("password").authorities("ROLE_USER");  // used for tests
        //auth.inMemoryAuthentication().withUser("admin").password("password").authorities("ROLE_ADMIN");
        auth.userDetailsService(userDetailsService);  //TODO - add BCrypt

    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
            http
                    .csrf().disable()
                    .authorizeRequests()
                        .antMatchers("/").permitAll()
                        .antMatchers("/product").permitAll()
                        .antMatchers("/products/*").permitAll()
                        .antMatchers("/shoppingcart").permitAll()
                        .antMatchers("/shoppingcart/*").permitAll()
                        .antMatchers("/contact").permitAll()
                        .antMatchers("/terms").permitAll()
                        .antMatchers("/resources/**").permitAll()
                        .antMatchers("/login").permitAll()
                        .antMatchers("/register").permitAll()
                        .antMatchers("/admin/products/s/save").permitAll()
                        .antMatchers("/admin/**").hasRole("ADMIN")
                        .anyRequest().authenticated()
                         .and()
                    .formLogin()
                        .loginPage("/login")
                        .permitAll()
                        .usernameParameter("email")  // got to be changed from default "user"
                        .passwordParameter("password")
                        .and()
                    .logout()
                        .logoutUrl("/logout")
                        .invalidateHttpSession(true)
                        .deleteCookies("JSESSIONID")
                        .logoutSuccessUrl("/contact")
                        .and()
                    .httpBasic();

        }
    }

