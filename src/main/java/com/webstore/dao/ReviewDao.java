package com.webstore.dao;

import com.webstore.model.Review;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Jarpen on 2016-07-17.
 */
@Repository
public interface ReviewDao extends JpaRepository<Review, Long> {
}
