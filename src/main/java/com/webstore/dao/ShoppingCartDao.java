package com.webstore.dao;

import com.webstore.model.Product;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.context.annotation.ScopedProxyMode.TARGET_CLASS;


@Repository
@Scope(value="session", proxyMode = TARGET_CLASS)  // an instance per web session
public class ShoppingCartDao {

    private List<Product> shoppingCart = new ArrayList<>();

    public void addProduct(Product product) {
        shoppingCart.add(product);
    }

    public List<Product> getCart() {
        List<Product> cart = shoppingCart;
        return cart;
    }

    public void setCart(List<Product> shoppingCart) {
        this.shoppingCart = shoppingCart;
    }

    public void remove(long index) {
        shoppingCart.remove((int)index);
    }

    public void clear() {
        shoppingCart.clear();
    }
}
