package com.webstore.dao;

import com.webstore.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Jarpen on 2016-07-17.
 */
@Repository
public interface UserDao extends JpaRepository<User,Long> {


    User findByEmail(String email);
}
