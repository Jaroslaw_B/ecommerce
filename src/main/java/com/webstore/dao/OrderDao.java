package com.webstore.dao;

import com.webstore.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Jarpen on 2016-07-17.
 */
@Repository
public interface OrderDao extends JpaRepository<Order, Long> {
}
