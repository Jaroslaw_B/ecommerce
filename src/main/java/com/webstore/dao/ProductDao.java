package com.webstore.dao;

import com.webstore.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Jarpen on 2016-07-17.
 */
@Repository
public interface ProductDao extends JpaRepository<Product, Long> {

    List<Product> findByCategory(String category);

}
