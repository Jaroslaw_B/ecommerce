package com.webstore.controller;

import com.webstore.dao.ProductDao;
import com.webstore.dao.UserDao;
import com.webstore.model.Product;
import com.webstore.model.User;
import com.webstore.service.ProductService;
import com.webstore.service.UserService;
import com.webstore.service.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.xml.bind.annotation.XmlRegistry;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Jarpen on 2016-07-17.
 */
@Controller
public class MainController {

    @Autowired
    ProductService productService;

    @Autowired
    UserService userService;

    @Autowired
    UserValidator userValidator;


    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String viewMain(Model model) {
        List<Product> productList = productService.findAll();

        //TODO change list of categories on sth better
        Set<String> categories = new HashSet();
        for(Product product : productList) {
            categories.add(product.getCategory());
        }
        model.addAttribute("productList", productList);
        model.addAttribute("categories", categories);
        return "main";
    }

    @RequestMapping(value="/contact", method = RequestMethod.GET)
    public String showContact() {
        return "contact";
    }

    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public String showAdminPanel() {
        return "admin";
    }

    @RequestMapping(value = "/terms", method = RequestMethod.GET)
    public String showTerms() {
        return "terms";
    }

    @RequestMapping(value = "/cart", method = RequestMethod.GET)
    public String showShoppingCart() {
        return "cart";
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String showRegisterFrom() {
        return "register";
    }

    @RequestMapping(value="/login", method = RequestMethod.GET)
    public String showLoginForm() {
        return "login";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String registerNewUser(@ModelAttribute @Validated User user, BindingResult result, Model model) {

        if(result.hasErrors()) {
            model.addAttribute("errors", result.getAllErrors()); //getFieldErrors
            return "register";
        }
        userService.save(user);

        return "redirect:/login";
    }

    @InitBinder("user")
    public void initUserValidator(WebDataBinder binder) {
        binder.addValidators(userValidator);
    }


    @RequestMapping(value = "/admin/newsletter", method = RequestMethod.GET)
    public String showNewsletters() {
        return "admin_newsletter";
    }
}
