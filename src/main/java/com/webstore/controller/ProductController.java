package com.webstore.controller;

import com.webstore.model.Product;
import com.webstore.model.Review;
import com.webstore.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Set;

/**
 * Created by Jarpen on 2016-07-17.
 */
@Controller
public class ProductController {

    @Autowired
    ProductService productService;

    @RequestMapping(value = "/product", method = RequestMethod.GET)
    public String showProduct(@RequestParam long id, Model model) {
        Product product = productService.findOne(id);
        Set<Review> reviews = product.getReviews();

        model.addAttribute("reviews", reviews);
        model.addAttribute("product", product);
        return "product";
    }

    @RequestMapping(value = "/products/{category}", method = RequestMethod.GET)
    public String viewByCategory(@PathVariable String category, Model model) {

        List<Product> productList = productService.findByCategory(category);
        model.addAttribute("productList", productList);
        return "main";
    }

    @RequestMapping(value = "/admin/products", method=RequestMethod.GET)
    public String showProducts(Model model) {
        List<Product> productList = productService.findAll();
        model.addAttribute("productList", productList);
        return "admin_products";
    }


    @RequestMapping(value = "/admin/products/addedit", method = RequestMethod.GET)
    public String addProducts(Model model) {
        Product product = new Product();
        model.addAttribute("product", product);
        return "admin_product_addedit";
    }

    @RequestMapping(value = "/admin/products/addedit/{id}", method = RequestMethod.GET)
    public String editProducts(@PathVariable long id, Model model) {
        Product product = productService.findOne(id);
        model.addAttribute("product", product);
        return "admin_product_addedit";
    }

    @RequestMapping(value="/admin/products/s/save", method = RequestMethod.POST)
    public String saveProduct(@RequestParam("file") MultipartFile file, @ModelAttribute Product product) {
        if(product.getId()!=null) {
            Product productFetched = productService.findOne(product.getId());
            productFetched = product;
            productService.saveEdited(productFetched);
        } else {
            productService.saveNew(product, file);
        }
        return "redirect:/admin/products";
    }

    @RequestMapping(value ="/admin/products/delete", method = RequestMethod.POST)
    public String deleteProduct(@RequestParam long id) {
        productService.delete(id);
        return "redirect:/admin/products";
    }
}

