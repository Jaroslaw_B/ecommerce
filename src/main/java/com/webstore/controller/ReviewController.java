package com.webstore.controller;


import com.webstore.model.Product;
import com.webstore.model.Review;
import com.webstore.model.User;
import com.webstore.service.ProductService;
import com.webstore.service.ReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Jarpen on 2016-07-17.
 */
@Controller
public class ReviewController {

    @Autowired
    ReviewService reviewService;

    @Autowired
    ProductService productService;

    @RequestMapping(value = "/review/{product_number}", method = RequestMethod.POST)
    public String submitReview(@ModelAttribute Review review, @PathVariable Long product_number) {
        Product product = productService.findOne(product_number);
        review.setProduct(product);
        reviewService.save(review);
        return "redirect:/";
    }

}
