package com.webstore.controller;

import com.webstore.model.User;
import com.webstore.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Controller
public class UserController {

    @Autowired
    UserService userService;

    @RequestMapping(value = "/admin/users", method = RequestMethod.GET)
    public String showUsers (Model model) {
        List<User> userList = userService.getAll();
        model.addAttribute("users", userList);
        return "admin_users";
    }

    @RequestMapping(value = "/admin/users/{id}", method = RequestMethod.GET)
    public String editUser (@PathVariable long id, Model model) {
        User user = userService.findOne(id);
        model.addAttribute("user", user);
        return "admin_user_edit";
    }

    @RequestMapping(value = "/admin/users/delete", method = RequestMethod.POST)
    public String deleteUser (@RequestParam long id) {
        userService.delete(id);
        return "redirect:/admin/users";
    }

    @RequestMapping(value = "/admin/users/save", method = RequestMethod.POST)
    public String saveUser (@ModelAttribute User user) {
        userService.save(user);
        return "redirect:/admin/users";
    }
}
