package com.webstore.controller;

import com.webstore.model.Order;
import com.webstore.model.Product;
import com.webstore.service.ProductService;
import com.webstore.service.ShoppingCartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;

/**
 * Created by Jarpen on 2016-07-20.
 */
@Controller
public class ShoppingCartController {

    @Autowired
    ShoppingCartService shoppingCartService;

    @Autowired
    ProductService productService;

    @RequestMapping(value = "/shoppingcart")
    public String showShoppingCart(Model model) {
        model.addAttribute("cart", shoppingCartService.getCart());
        int ammount = BigDecimal.valueOf(5).intValue();
        for (int i=0; i<5; i++) {
            ammount += BigDecimal.valueOf(5).intValue();
            System.out.println(ammount);
        }
        return "cart";
    }

    @RequestMapping(value ="/shoppingcart/add", method = RequestMethod.POST)
    public String addToCar(@RequestParam long id) {
        Product product = productService.findOne(id);
        shoppingCartService.add(product);
        return "redirect:/";
    }

    @RequestMapping(value = "shoppingcart/delete", method = RequestMethod.POST)
    public String removeFromCart(@RequestParam long index) {
        System.out.println(index);
        shoppingCartService.remove(index);
        return "redirect:/shoppingcart";
    }

    @RequestMapping(value = "shoppingcart/clear", method = RequestMethod.GET)
    public String clearCart() {
        shoppingCartService.clear();
        return "redirect:/";
    }


}
