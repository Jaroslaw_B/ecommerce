package com.webstore.controller;

import com.webstore.model.Order;
import com.webstore.model.Product;
import com.webstore.model.User;
import com.webstore.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * Created by Jarpen on 2016-07-17.
 */
@Controller
public class OrderController {

    @Autowired
    OrderService orderService;

    @RequestMapping(value = "/order", method = RequestMethod.GET)
    public String order() {
        orderService.order();

        return "redirect:/";
    }

    @RequestMapping(value = "/admin/orders", method = RequestMethod.GET)
    public String showOrder(Model model) {
        List<Order> orderList = orderService.findAll();
        model.addAttribute("orderList", orderList);
        return "admin_orders";
    }

    @RequestMapping(value = "/order/confirm", method = RequestMethod.POST)
    public String confirmOrder(@RequestParam long id) {
        Order order = orderService.findById(id);
        order.setStatus(Order.Status.ACCEPTED);
        orderService.confirm(order);

        return"redirect:/admin/orders";
    }

    @RequestMapping(value = "/admin/order/details/{id}", method = RequestMethod.GET)
    public String showOrderDetails(@PathVariable long id, Model model) {
        Order fetched = orderService.findById(id);
        model.addAttribute("order", fetched);
        return "admin_order_details";
    }
}
