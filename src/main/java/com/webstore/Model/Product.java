package com.webstore.model;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

/**
 * Created by Jarpen on 2016-07-16.
 */
@Entity
@Table(name="products")
public class Product extends BaseEntity{

    @Column(name="name")
    private String name;

    @Column(name="category")
    private String category;

    @Column(name="price")
    private BigDecimal price;

    @Column(name="quantity")
    private int quantity;

    @Column(name="photo_url")
    private String photoUrl;

    @Column(name="description")
    private String description;

    @OneToMany(cascade=CascadeType.REMOVE, mappedBy = "product", fetch = FetchType.EAGER)
    private Set<Review> reviewList;

    @ManyToMany( mappedBy="productList",fetch=FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
    private List<Order> orderList;

    public Product() {
    }

    public Product(String name, String category, BigDecimal price, int quantity, String photoUrl, float discount, String description) {
        this.name = name;
        this.category = category;
        this.price = price;
        this.quantity = quantity;
        this.photoUrl = photoUrl;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Set<Review> getReviews() {
        return reviewList;
    }

    public void setReviews(Set<Review> reviews) {
        this.reviewList = reviews;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Order> getOrderList() {
        return orderList;
    }

    public void setOrderList(List<Order> orderList) {
        this.orderList = orderList;
    }
}
