package com.webstore.model;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

/**
 * Created by Jarpen on 2016-07-16.
 */
@Entity
@Table(name="users")
public class User extends BaseEntity{

    public enum Role {USER, WORKER, ADMIN};

    @Column(name="email", unique = true)
    private String email;

    @Column(name="password")
    private String password;

    @Column(name="full_name") //shouldn't be first name and second name in one cell (normalization)
    private String fullName;

    @Column(name="address")
    private String address;

    @Column(name="role")
    @Enumerated(EnumType.STRING)
    private Role role;

    @Column(name="newsletter_acceptance", columnDefinition = "TINYINT(1)")
    private boolean nwsltr;

    @OneToMany(cascade=CascadeType.REMOVE, mappedBy = "user", fetch = FetchType.EAGER)
    private Set<Review> reviews;

    @OneToMany(cascade=CascadeType.REMOVE, mappedBy = "user")
    private List<Order> orders;

    public User() {
        this.role = Role.USER;
    }

    public User(String email, String fullName, String address, Role role, boolean nwsltr) {
        this.email = email;
        this.fullName = fullName;
        this.address = address;
        this.role = role;
        this.nwsltr = nwsltr;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public boolean isNwsltr() {
        return nwsltr;
    }

    public void setNwsltr(boolean nwsltr) {
        this.nwsltr = nwsltr;
    }

    public Set<Review> getReviews() {
        return reviews;
    }

    public void setReviews(Set<Review> reviews) {
        this.reviews = reviews;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }
}
