package com.webstore.model;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "orders")
public class Order extends BaseEntity{

    public enum Status {ORDERED, ACCEPTED};

    @Column(name = "order_date")
    private Date orderDate;

    @Column(name="status")
    @Enumerated(EnumType.STRING)
    private Status status;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToMany(fetch=FetchType.EAGER)
    @JoinTable(name = "order_product", joinColumns = { @JoinColumn(name = "order_id") }, inverseJoinColumns = { @JoinColumn(name = "product_id") })
    @Fetch(FetchMode.SELECT) // FetchMode.SELECT increases number of SQL queries fired by Hibernate but ensures only one instance per root entity record.
    private List<Product> productList;

    @Transient
    double ammount;

    public Order() {
        this.orderDate = new Date();
        this.status = Status.ORDERED;
    }

    public double getAmmount() {
        for (Product product : productList) {
            this.ammount += product.getPrice().doubleValue();
        }
        return ammount;
    }

    public void setAmmount(double ammount) {
        this.ammount = ammount;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Product> getProductList() {
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }
}
