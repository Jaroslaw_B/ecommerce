package com.webstore.service;


import com.webstore.model.Review;

public interface ReviewService {

    void save(Review review);
}
