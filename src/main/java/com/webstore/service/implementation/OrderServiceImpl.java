package com.webstore.service.implementation;

import com.webstore.dao.OrderDao;
import com.webstore.dao.ProductDao;
import com.webstore.dao.ShoppingCartDao;
import com.webstore.dao.UserDao;
import com.webstore.model.Order;
import com.webstore.model.Product;
import com.webstore.model.User;
import com.webstore.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by Jarpen on 2016-07-19.
 */
@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    OrderDao orderDao;

    @Autowired
    ShoppingCartDao shoppingCartDao;

    @Autowired
    ProductDao productDao;

    @Autowired
    UserDao userDao;

    @Override
    public void order() {
        Order order = new Order();
        List<Product> cart = shoppingCartDao.getCart();
        order.setProductList(cart);

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//        String email = auth.getName();
//        User user = userDao.findByEmail(email);
//        order.setUser(user);
        order.setUser(userDao.findByEmail(auth.getName()));

        for (Product product: cart) {
            Product fetched = productDao.findOne(product.getId());
            fetched.setQuantity(fetched.getQuantity()-1);
            productDao.save(fetched);
        }
        orderDao.save(order);

    }

    @Override
    public List<Order> findAll() {
        List<Order> orderList = orderDao.findAll();
        return orderList;
    }

    @Override
    public Order findById(long id) {
        Order order = orderDao.findOne(id);
        return order;
    }

    @Override
    public void confirm(Order order) {
        orderDao.save(order);
    }


}
