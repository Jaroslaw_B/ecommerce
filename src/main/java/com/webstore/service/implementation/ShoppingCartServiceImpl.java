package com.webstore.service.implementation;

import com.webstore.dao.ShoppingCartDao;
import com.webstore.model.Product;
import com.webstore.service.ShoppingCartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ShoppingCartServiceImpl implements ShoppingCartService {

    @Autowired
    ShoppingCartDao shoppingCartDao;

    @Override
    public void add(Product product) {
        shoppingCartDao.addProduct(product);
    }

    @Override
    public List<Product> getCart() {
        return shoppingCartDao.getCart();
    }

    @Override
    public void remove(long index) {
        shoppingCartDao.remove(index);
    }

    @Override
    public void clear() {
        shoppingCartDao.clear();
    }

}
