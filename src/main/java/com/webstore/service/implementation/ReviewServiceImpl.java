package com.webstore.service.implementation;

import com.webstore.dao.ReviewDao;
import com.webstore.dao.UserDao;
import com.webstore.model.Review;
import com.webstore.model.User;
import com.webstore.service.ReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;


@Service
public class ReviewServiceImpl implements ReviewService {

    @Autowired
    ReviewDao reviewDao;

    @Autowired
    UserDao userDao;

    @Override
    public void save(Review review) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String email = auth.getName();
        User user = userDao.findByEmail(email);

        review.setUser(user);
        reviewDao.save(review);
    }
}
