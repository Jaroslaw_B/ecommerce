package com.webstore.service.implementation;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import com.webstore.dao.ProductDao;
import com.webstore.model.Product;
import com.webstore.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    ProductDao productDao;

    @Autowired
    private Cloudinary cloudinary;


    @Override
    public List<Product> findAll(){
        List<Product> productList = productDao.findAll();
        return productList;
    }

    @Override
    public Product findOne(long id) {
        Product product = productDao.findOne(id);
        return product;
    }

    @Override
    public List<Product> findByCategory(String category) {
        List<Product> productList = productDao.findByCategory(category);
        return productList;
    }

    @Override
    public void delete(long id) {
        productDao.delete(id);
    }

    @Override
    public void save(Product product) {
        productDao.save(product);
    }

    @Override
    public void saveNew(Product product, MultipartFile file) {
        try {
            Map uploadResult = cloudinary.uploader().upload(convert(file), ObjectUtils.emptyMap());
            String photo_url = (String) uploadResult.get("url");
            product.setPhotoUrl(photo_url);
        } catch (IOException e) {
            e.printStackTrace();
        }
        productDao.save(product);
    }

    @Override
    public void saveEdited(Product productFetched) {
        productDao.save(productFetched);
    }

    public File convert(MultipartFile file) throws IOException {
        File convFile = new File(file.getOriginalFilename());
        convFile.createNewFile();
        FileOutputStream fos = new FileOutputStream(convFile);
        fos.write(file.getBytes());
        fos.close();
        return convFile;
    }

}
