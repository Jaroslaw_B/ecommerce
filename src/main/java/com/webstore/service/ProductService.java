package com.webstore.service;

import com.webstore.dao.ProductDao;
import com.webstore.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * Created by Jarpen on 2016-07-17.
 */

public interface ProductService {


    Product findOne(long id);

    public List<Product> findAll();

    List<Product> findByCategory(String category);

    void delete(long id);

    void save(Product product);

    void saveNew(Product product, MultipartFile file);

    void saveEdited(Product productFetched);
}
