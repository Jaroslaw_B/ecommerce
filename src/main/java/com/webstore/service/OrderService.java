package com.webstore.service;

import com.webstore.model.Order;
import com.webstore.model.Product;
import com.webstore.model.User;

import java.util.List;

/**
 * Created by Jarpen on 2016-07-17.
 */

public interface OrderService {

    void order();

    List<Order> findAll();

    Order findById(long id);

    void confirm(Order order);

}
