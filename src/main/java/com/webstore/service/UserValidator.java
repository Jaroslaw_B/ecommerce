package com.webstore.service;

import com.webstore.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class UserValidator implements Validator{

    @Autowired
    UserService userService;

    @Override
    public boolean supports(Class<?> aClass) {
        return User.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "Email field cant'be empty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "Password field cant'be empty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "fullName", "Name field cant'be empty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "address", "Addres field cant'be empty");

        User user = (User) target;

        if(userService.userExists(user)) {
            errors.reject("User exists in database");
        }
    }
}
