package com.webstore.service;


import com.webstore.model.Product;

import java.util.List;

public interface ShoppingCartService {
    void add(Product product);

    List<Product> getCart();

    void remove(long index);

    void clear();
}
