# Basic ecommerce engine. #

## Technology ##
* Spring MVC, Spring Security, Spring Data
* Hibernate, MySQL
* JSP, JSTL, Spring Security Taglibs
* Bootstrap, Javascript
* Tomcat 7.0

## Features ##
* Registration
* Logging
* Admin panel
*   Adding/deleting products 
*   Photos of products are uploading on external service Cloudinary. In database stored only url.
*   Editing/deleting users. Granting roles and changing newsletter acceptance.
*   Details and order confirmation
* Session scope shopping cart
* Reviews with star rating system

## Running ##
* Frontend - http://localhost:8081/
* Database (mysql) - 127.0.0.1:3306/webstore